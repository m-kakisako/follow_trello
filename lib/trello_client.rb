require "active_support"
require "active_support/core_ext"
require "trello"

class TrelloClient
  def initialize
    unset_env?
    Trello.configure do |trello_config|
      trello_config.developer_public_key = ENV["TRELLO_API_KEY"]
      trello_config.member_token = ENV["TRELLO_API_TOKEN"]
    end
  end

  def board_list
    ret = []
    ret.push([*"id", "name", "url"])
    Trello::Board.all.each do |board|
      ret.push([
        board.id,
        board.name,
        board.url
      ])
    end
    ret
  end

  def list_by_board_id(board_id)
    lists = Trello::Board.find(board_id).lists
    ret = []
    ret.push([*"id", "name"])
    lists.each do |list|
      ret.push([
        list.id,
        list.name
      ])
    end
    ret
  end

  def card_list_by_list_id(list_id)
    cards = Trello::List.find(list_id).cards
    ret = []
    ret.push([*"id", "name", "url"])
    cards.each do |card|
      ret.push([
        card.id,
        card.name,
        card.short_url
      ])
    end
    ret
  end

  private

  def unset_env?
    env = %w(
      TRELLO_API_KEY
      TRELLO_API_TOKEN
    )
    ret = empty_env(env)
    abort "以下の環境変数が設定されておりません\n#{ret}" unless ret.empty?
  end

  def empty_env(env)
    ret = []
    env.each do |val|
      ret.push(val) if ENV[val].nil?
    end
    ret
  end
end
