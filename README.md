# FollowTrello

## 概要
TrelloAPIを使用して、TrelloのボードID等の情報を取得します。

## 準備

### このツールを使用するには、TrelloAPIキーとトークンが必要です。
- APIキーとトークン取得については下記を参照
http://qiita.com/isseium/items/8eebac5b79ff6ed1a180

### 環境変数の設定
- 実行前に環境変数を設定してください
```
TRELLO_API_KEY=your trello api key
TRELLO_API_TOKEN=your trello api token
export TRELLO_API_KEY TRELLO_API_TOKEN
```

## 実行方法

### 使用できるコマンドの確認
```
$ bundle exec rake -T
rake trello:board_list                     # disp board list
rake trello:card_list_by_list_id[list_id]  # disp board list's card by list_id
rake trello:list_by_board_id[board_id]     # disp board's list by board_id
```

### ex)ボード一覧を取得する
```
$ bundle exec rake trello:board_list
+------------------------+--------------------------------+-------------------------------------------+
|id                      |name                            |url                                        |
|123123123123123123123123|MyTask                          |https://trello.com/b/123123/mytask       |
|456456456456456456456456|Welcome Board                   |https://trello.com/b/456456/welcome-board|
+------------------------+--------------------------------+-------------------------------------------+
2 row(s)
```

### ex)ボードのリスト一覧を表示する
```
$ bundle exec rake trello:list_by_board_id[456456456456456456456456]
+------------------------+------------+
|id                      |name        |
|456456456456456456456450|Basics      |
|456456456456456456456451|Intermediate|
|456456456456456456456452|Advanced    |
+------------------------+------------+
3 row(s)
```
